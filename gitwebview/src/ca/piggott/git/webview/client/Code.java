package ca.piggott.git.webview.client;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Widget;

public class Code extends Widget {
	private final String type;
	
	public Code(String content, String type) {
		setElement(Document.get().createPreElement());
		getElement().setInnerText(content);
		getElement().addClassName("filecontent");
		getElement().addClassName("brush: "+type);
		this.type = type;
//		triggerSyntaxHighlight(getElement());
	}
	
	protected void onAttach() {
		super.onAttach();
		try {
			triggerSyntaxHighlight(getElement(), type);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private native void triggerSyntaxHighlight(Element element, String type)  /*-{
		$wnd.gitview.loadBrush(type, function(){
			$wnd.SyntaxHighlighter.highlight(null, element);
		});
	}-*/;
}
