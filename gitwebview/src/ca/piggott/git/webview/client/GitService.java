package ca.piggott.git.webview.client;

import java.util.List;

import ca.piggott.git.webview.shared.Branch;
import ca.piggott.git.webview.shared.File;
import ca.piggott.git.webview.shared.FileSummary;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("git")
public interface GitService extends RemoteService {
	
	List<Branch> getBranches(String repo);

	File getFile(String repo, String refid, String path);
	
	List<String> getRepositories();
	
	String getTags(String repo);
	
	List<FileSummary> getTree(String repo, String path);
	
	List<FileSummary> getTree(String repo, String refid, String path);
}
