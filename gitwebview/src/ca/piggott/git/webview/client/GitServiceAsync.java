package ca.piggott.git.webview.client;

import java.util.List;

import ca.piggott.git.webview.shared.Branch;
import ca.piggott.git.webview.shared.File;
import ca.piggott.git.webview.shared.FileSummary;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface GitServiceAsync {

	void getBranches(String repo, AsyncCallback<List<Branch>> callback);

	void getTags(String repo, AsyncCallback<String> callback);

	void getRepositories(AsyncCallback<List<String>> callback);

	void getTree(String repo, String path, AsyncCallback<List<FileSummary>> callback);

	void getTree(String repo, String refid, String path, AsyncCallback<List<FileSummary>> callback);

	void getFile(String repo, String refid, String path,
			AsyncCallback<File> callback);

}
