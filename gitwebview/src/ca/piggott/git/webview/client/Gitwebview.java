package ca.piggott.git.webview.client;

import ca.piggott.git.webview.client.components.TreeComponent;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.ScrollPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Gitwebview implements EntryPoint, ValueChangeHandler<String> {
	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	public static final GitServiceAsync gitService = GWT.create(GitService.class);

	private ScrollPanel panel;
	
	private Composite currentView; 

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		this.panel = new ScrollPanel();
		this.panel.setStyleName("content");
		RootLayoutPanel.get().add(this.panel);
		History.addValueChangeHandler(this);
		History.fireCurrentHistoryState();
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		// history
		String historyToken = event.getValue();
		final String[] tokens = historyToken == null ? new String[0] : historyToken.split("/");
		if (tokens.length == 0 || tokens.length == 1 && tokens[0].isEmpty()) {
			this.currentView = new RepositoryList();
			this.panel.add(this.currentView);
		} else if (tokens.length > 1 && tokens[1].equals("tree")) {
			if (!(currentView instanceof TreeComponent)) {
				currentView = new TreeComponent();
			}

			final String repo = tokens[0];
			final String refid = tokens.length > 2 ? tokens[2] : "master";
			final String path =  tokens.length > 3 ? historyToken.substring(repo.length() + refid.length() + 6) : "/"; 

			((TreeComponent)currentView).setLocation(repo, refid, path);

			this.panel.clear();
			this.panel.add(currentView);
		}
	}
	
	public static String createTreeUrl(String repo, String commit, String path, boolean isDirectory) {
		StringBuilder sb = new StringBuilder(repo).append('/').append("tree").append('/').append(commit);
		if (!path.startsWith("/")) {
			sb.append('/');
		}
		sb.append(path);
		if (isDirectory && !path.endsWith("/")) {
			sb.append('/');
		}
		return sb.toString();
	}
}
