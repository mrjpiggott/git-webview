package ca.piggott.git.webview.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;

public class RepositoryList extends Composite implements AsyncCallback<List<String>> {

	private SimplePanel body;

	public RepositoryList() {
		body = new SimplePanel();
		initWidget(body);
		body.add(new Label("Loading Repository List"));
		Gitwebview.gitService.getRepositories(this);
	}


	@Override
	public void onFailure(Throwable caught) {
		
	}

	@Override
	public void onSuccess(List<String> result) {
		Grid grid = new Grid(result.size(), 1);

		int x = 0;
		for (String repo : result) {
			grid.setWidget(x++, 0, new Hyperlink(repo, Gitwebview.createTreeUrl(repo, "master", "/", true)));
		}

		body.clear();
		body.add(grid);
	}

}
