package ca.piggott.git.webview.client;

import java.util.List;

import ca.piggott.git.webview.shared.FileSummary;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;

public class RepositoryTreeView extends Composite implements AsyncCallback<List<FileSummary>> {

	private VerticalPanel body;

	private String repo;

	private String path;

	private String commit;

	public RepositoryTreeView(String repo, String commit) {
		this(repo, commit, "/");
	}

	public RepositoryTreeView(String repo, String commit, String path) {
		body = new VerticalPanel();
		initWidget(body);
		Gitwebview.gitService.getTree(repo, commit, path, this);
		this.repo = repo;
		this.path = path.endsWith("/") ? path : path + '/';
		this.commit = commit;
	}

	@Override
	public void onFailure(Throwable caught) {
		
	}

	@Override
	public void onSuccess(List<FileSummary> result) {
		body.clear();
		Grid grid = new Grid(result.size(), 2);
		body.add(grid);
		
		int i=0;
		for (FileSummary file : result) {
			Hyperlink link = new Hyperlink(file.getFilename(), Gitwebview.createTreeUrl(repo, commit, path + file.getFilename(), file.isDirectory()));
			if (file.isDirectory()) {
				link.setStyleName("directory");
			} else {
				link.setStyleName("file");
			}
			grid.setWidget(i, 0, link);
			grid.setText(i++, 1, file.getRefid());
		}
	}
}
