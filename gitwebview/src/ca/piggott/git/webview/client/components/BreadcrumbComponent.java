package ca.piggott.git.webview.client.components;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;

public class BreadcrumbComponent extends Composite {
	
	public static final String HEIGHT = "2.0EM";

	private HorizontalPanel hPanel = new HorizontalPanel();

	public BreadcrumbComponent(){
		
	}

	public BreadcrumbComponent(String repo, String commit, String filepath) {
		initWidget(hPanel);
		hPanel.setStyleName("breadcrumb");
		hPanel.setHeight(HEIGHT);
		final String[] segments = (filepath.startsWith("/") ? filepath.substring(1) : filepath).split("/");
		hPanel.add(new Hyperlink(repo, createUrl(repo, commit, segments, 0)));

		for (int i=0; i<segments.length; i++) {
			hPanel.add(new Label(" / "));
			if (i+1 == segments.length) {
				hPanel.add(new Label(segments[i]));
			} else {
				hPanel.add(new Hyperlink(segments[i], createUrl(repo, commit, segments, 1+i)));
			}
		}
	}
	
	public void setLocation(String repo, String commit, String filepath) {
		
	}
	
	private static String createUrl(String repo, String commit, String[] segments, int length) {
		StringBuilder sb = new StringBuilder(repo).append('/');
		sb.append(commit).append('/');
		for (int i=0; i<length; i++) {
			sb.append(segments[i]).append('/');
		}
		return sb.toString();
	}
}
