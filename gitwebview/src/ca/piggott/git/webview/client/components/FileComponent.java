package ca.piggott.git.webview.client.components;

import ca.piggott.git.webview.client.Code;
import ca.piggott.git.webview.client.Gitwebview;
import ca.piggott.git.webview.shared.File;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;

public class FileComponent extends Composite implements AsyncCallback<File> {

	private SimplePanel panel;

	private String type;

	private String path;

	private String refid;

	private String repo;

	public FileComponent(String repo, String filepath) {
		this(repo, "master", filepath);
	}
	
	public FileComponent(String repo, String refid, String filepath) {
		this.repo = repo;
		this.refid = refid;
		this.path = filepath;
		
		panel = new SimplePanel();
		initWidget(panel);
		panel.setStyleName("filecontent");
		Gitwebview.gitService.getFile(getRepo(), refid, getPath(), this);

		int last = getPath().lastIndexOf(".");
		if (last != -1) {
			type = getPath().substring(last+1);
		}
	}

	public String getPath() {
		return path;
	}

	public String getRefid() {
		return refid;
	}

	public String getRepo() {
		return repo;
	}

	@Override
	public void onFailure(Throwable caught) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSuccess(File result) {
		if (type != null) {
			panel.add(new Code(result.getContent(), type));
		} else {
			panel.add(new Label(result.getContent()));
		}		
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public void setRepo(String repo) {
		this.repo = repo;
	}
}
