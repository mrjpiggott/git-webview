package ca.piggott.git.webview.client.components;

import java.util.List;

import ca.piggott.git.webview.client.Gitwebview;
import ca.piggott.git.webview.client.RepositoryTreeView;
import ca.piggott.git.webview.shared.Branch;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class TreeComponent extends Composite {

	private VerticalPanel panel;
	
	private HorizontalPanel breadcrumbs;
	
	private SimplePanel body;
	
	private ListBox commitList;
	
	private String repo;

	private String path;

	private String refid;

	public TreeComponent() {
		panel = new VerticalPanel();
		initWidget(panel);
		
		panel.add(getMenuBar());
		breadcrumbs = new HorizontalPanel();
		breadcrumbs.setStyleName("breadcrumb");
		breadcrumbs.setHeight("2.0EM");
		panel.add(breadcrumbs);
		body = new SimplePanel();
		panel.add(body);
	}
	
	private Widget getMenuBar() {
		commitList = new ListBox();
		commitList.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				History.newItem(Gitwebview.createTreeUrl(getRepo(), commitList.getItemText(commitList.getSelectedIndex()), getPath(), getPath().endsWith("/")));
			}
		});
		return commitList;
	}

	public void setLocation(String repo, String refid, String path) {
		this.setRepo(repo);
		this.setRefid(refid);
		this.path = path;

		if (!path.endsWith("/")) {
			body.clear();
			body.add(new FileComponent(repo, refid, path));
		} else {
			body.clear();
			body.add(new RepositoryTreeView(repo, refid, path));
		}
		updateBreadcrumbs();
		Gitwebview.gitService.getBranches(repo, new AsyncCallback<List<Branch>>() {
			@Override
			public void onSuccess(List<Branch> result) {
				for (Branch branch : result) {
					commitList.addItem(branch.getName());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}

	private void updateBreadcrumbs() {
		breadcrumbs.clear();
		final String[] segments = (path.startsWith("/") ? path.substring(1) : path).split("/");
		breadcrumbs.add(new Hyperlink(this.getRepo(), createUrl(this.getRepo(), this.getRefid(), segments, 0)));

		for (int i=0; i<segments.length; i++) {
			breadcrumbs.add(new Label(" / "));
			if (i+1 == segments.length) {
				breadcrumbs.add(new Label(segments[i]));
			} else {
				breadcrumbs.add(new Hyperlink(segments[i], createUrl(this.getRepo(), this.getRefid(), segments, 1+i)));
			}
		}
	}

	public String getPath() {
		return path;
	}

	public String getRepo() {
		return repo;
	}

	public void setRepo(String repo) {
		this.repo = repo;
	}

	public String getRefid() {
		return refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	private static String createUrl(String repo, String commit, String[] segments, int length) {
		StringBuilder sb = new StringBuilder("/");
		for (int i=0; i<length; i++) {
			sb.append(segments[i]).append('/');
		}
		return Gitwebview.createTreeUrl(repo, commit, sb.toString(), true);
	}
}
