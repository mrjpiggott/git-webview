package ca.piggott.git.webview.server;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.FileMode;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevTag;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.eclipse.jgit.treewalk.filter.TreeFilter;

import ca.piggott.git.webview.client.GitService;
import ca.piggott.git.webview.server.util.Conversion;
import ca.piggott.git.webview.shared.Branch;
import ca.piggott.git.webview.shared.File;
import ca.piggott.git.webview.shared.FileSummary;

import com.google.gwt.dev.util.collect.HashMap;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class GitServiceImpl extends RemoteServiceServlet implements GitService {
	private Map<String,Git> repos;

	@Override
	public void init() throws ServletException {
		Map<String,Git> r = new HashMap<String,Git>();
		try {
			r.put("jgit", Git.open(new java.io.File("G:\\scm\\jgit")) );
			repos = Collections.unmodifiableMap(r);
		} catch (IOException e) {
			throw new ServletException(e);
		}
	}

	@Override
	public List<Branch> getBranches(String repoName) {
		Git repo = repos.get(repoName);
		if (repo == null) 
			throw new IllegalArgumentException("Unknown repository: " + repoName);
		
		List<Branch> branches = new LinkedList<Branch>();
		
		for (Ref ref : repo.branchList().call()) {
			branches.add(Conversion.toBranch(ref));
		}
		
		for (RevTag ref : repo.tagList().call()) {
			branches.add(Conversion.toBranch(ref));
		}
		return branches;
	}

	@Override
	public String getTags(String repo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getRepositories() {
		return new LinkedList<String>(repos.keySet());
	}

	@Override
	public List<FileSummary> getTree(String repoName, String path) {
		Git repo = getGit(repoName);

		ObjectId obj = getObjectId(repo, "master");

		return getTree(repo, path, obj);
	}

	@Override
	public List<FileSummary> getTree(String repoName, String refid, String path) {
		Git repo = getGit(repoName);
		ObjectId objectId = getObjectId(repo, refid);
		return getTree(repo, path, objectId);
	}

	private List<FileSummary> getTree(Git repo, String path, ObjectId objectId) {
		List<FileSummary> files = new LinkedList<FileSummary>();
		try {
			ObjectReader reader = repo.getRepository().newObjectReader();
			CanonicalTreeParser dir = locateParser(reader, repo, path, objectId);
			RevWalk rw = new RevWalk(repo.getRepository());
			if ( !"/".equals(path))
				rw.setTreeFilter(PathFilter.create(path));
			rw.
			if (dir != null) {
				while(!dir.eof()) {
					byte[] buffer = new byte[dir.getNameLength()];
					dir.getName(buffer, 0);
					files.add(new FileSummary(dir.getEntryObjectId().getName(), new String(buffer), new Date(rw.parseCommit(dir.getEntryObjectId()).getCommitTime()), FileMode.TREE.equals(dir.getEntryFileMode()) ));
					if (dir != ( dir = dir.next()) ) {
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalStateException(e.getMessage());
		}
		Collections.sort(files);
		return files;
	}
	
	private Git getGit(String repoName) {
		Git repo = repos.get(repoName);
		if (repo == null) 
			throw new IllegalArgumentException("Unknown repository: " + repoName);
		return repo;
	}

	@Override
	public File getFile(String repoName, String refid, String path) {
		Git repo = getGit(repoName);
		ObjectId obj = getObjectId(repo, refid);
		ObjectReader reader = repo.getRepository().newObjectReader();
		CanonicalTreeParser parser = locateParser(reader, repo, path, obj);
		try {
			ObjectLoader loader = reader.open(parser.getEntryObjectId());
			
			return new File(parser.getEntryObjectId().toString(), path,  new String(loader.getBytes()), null);
		} catch (MissingObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private CanonicalTreeParser locateParser(final ObjectReader reader, Git repo, String path, ObjectId objectId) {
		final String[] pathSegments = (path.startsWith("/") && path.length() > 1 ? path.substring(1) : path).split("/");
		try {
			int depth = 0;
			CanonicalTreeParser parser = new CanonicalTreeParser(null, reader, new RevWalk(repo.getRepository()).parseTree(objectId));
			while (!parser.eof() && depth < pathSegments.length) {
				byte[] buffer = new byte[parser.getNameLength()];
				parser.getName(buffer, 0);
				final String curName = new String(buffer);
				if (curName.equals(pathSegments[depth])) {
					if (FileMode.TREE.equals(parser.getEntryFileMode())) {
						parser = parser.createSubtreeIterator(reader);
					} else if (depth+1 < pathSegments.length) {
						throw new IllegalStateException("Invalid path: " + path);
					}
					++depth;
				} else {
					parser = parser.next();
				}
			}
			return parser;
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalStateException(e.getMessage());
		}
	}
	
	private ObjectId getObjectId(Git repo, String refid) {
		if (refid.length() == Constants.OBJECT_ID_STRING_LENGTH) {
			return ObjectId.fromString(refid);
		} else {
			final String branch = "refs/heads/" + refid;
			for (Ref ref : repo.branchList().call()) {
				if (branch.equals(ref.getName())) {
					return ref.getObjectId();
				}
			}
			
			for (RevTag tag : repo.tagList().call()) {
				if (tag.getTagName().equals(refid)) {
					return tag.toObjectId();
				}
			}
		}
		throw new IllegalStateException("Missing " + refid);
	}
}
