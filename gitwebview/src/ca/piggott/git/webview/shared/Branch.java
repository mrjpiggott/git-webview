package ca.piggott.git.webview.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Branch implements IsSerializable {

	private String name;

	private String objectId;

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
